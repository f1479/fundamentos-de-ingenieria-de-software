### Integrantes:
1. Bryan Felipe Muñoz Molina  20162020408
2. Cristian Javier Martinez Blanco  20182020155
3. Kevin Santiago Holguín Bello 20191020035

# Historias de Usuario

### Cliente,(tendero, ayudantes),domiciliario

### Domiciliario

| **Título:**|**Id:** 1|
|----------------------------|--------------------------------|
| **Historia de usuario:** Como **domiciliario** quiero que me muestre con geolocalización de los clientes para poder llegar más rápido y tener mayor eficiencia en las entregas.||
| **Prioridad:** Media |**Estimación (días):** 3 días |

| **Título:**|**Id:** 2|
|----------------------------|--------------------------------|
| **Historia de usuario:** Como **domiciliario** quiero que mi mapa de geolocalización me muestre una ruta eficiente para hacer las entregas respecto a la cantidad de clientes que están en espera de recibir su pedido de una manera más rápida.||
| **Prioridad:** Media |**Estimación (días):** 10 días |

| **Título:**|**Id:** 3|
|----------------------------|--------------------------------|
| **Historia de usuario:** Como **domiciliario** quiero poder escanear el código QR que se le genera al cliente en el momento de finalizar la compra para confirmar el pedido, y hacer una entrega segura respecto a lo que se debe entregar.||
| **Prioridad:** Alta |**Estimación (días):** 1 días |

| **Título:**|**Id:** 4|
|----------------------------|--------------------------------|
| **Historia de usuario:** Como **domiciliario** quiero que poder recibir pagos electronicos por medio de NFC de mi celular para asi no tener inconvenientes a la hora de que los clientes pagan su pedidos.||
| **Prioridad:** Media |**Estimación (días):** 10 días |



### Tendero

| **Título:**|**Id:** 5|
|----------------------------|--------------------------------|
| **Historia de usuario:** Como **tendero** quiero ver resultados estadísticos de las ventas diarias, mensuales, anuales y totales, para saber qué productos son más vendidos y cuáles generan más ganancia, para así poder modificar de acuerdo a estos datos mi repertorio de productos.||
| **Prioridad:** Alta |**Estimación (días):** 5 días |

| **Título:**|**Id:** 6|
|----------------------------|--------------------------------|
| **Historia de usuario:** Como **tendero** quiero que me notifique todas las compras que se realicen en la plataforma web.||
| **Prioridad:** Alta |**Estimación (días):** 1 día |

| **Título:**|**Id:** 7|
|----------------------------|--------------------------------|
| **Historia de usuario:** Como **tendero** quiero consultar, modificar, agregar, eliminar productos cuando el cliente esta facturando en caja para asi poder desempeñar mi trabajo sin iterrupciones.||
| **Prioridad:** Alta |**Estimación (días):** 3 días |

| **Título:**|**Id:** 8|
|----------------------------|--------------------------------|
| **Historia de usuario:** Como **tendero** deseo consultar la disponibilidad de productos para asi poder saber que productos vender.||
| **Prioridad:** Alta |**Estimación (días):** 1 día |

### Cliente


| **Título:**|**Id:** 9|
|----------------------------|--------------------------------|
| **Historia de usuario:** Como **cliente** quiero poder acceder a una tarjeta preferencial para realizar mis compras por la plataforma.||
| **Prioridad:** Media |**Estimación (días):** 30 días |

| **Título:**|**Id:** 10|
|----------------------------|--------------------------------|
| **Historia de usuario:** Como **cliente** deseo poder acumular puntos en mis compras para que se puedan utilizar a futuro.||
| **Prioridad:** Baja |**Estimación (días):** 1 día |

| **Título:**|**Id:** 11|
|----------------------------|--------------------------------|
| **Historia de usuario:** Como **cliente** deseo que la plataforma me genere recomendaciones con base en mis preferencias para futuras compras.||
| **Prioridad:** Media |**Estimación (días):** 10 días |

| **Título:**|**Id:** 12|
|----------------------------|--------------------------------|
| **Historia de usuario:** Como **cliente** deseo poder pagar con tarjeta o a través de puntos de pago como servientrega, baloto… etc.||
| **Prioridad:** Alta |**Estimación (días):** 30 días |

| **Título:**|**Id:** 13|
|----------------------------|--------------------------------|
| **Historia de usuario:** Como **cliente** quiero saber el estado de mi pedido: si fue visto, va en camino o ya fue entregado.||
| **Prioridad:** Alta |**Estimación (días):** 2 días |



### Ayudante

| **Título:**|**Id:** 14|
|----------------------------|--------------------------------|
| **Historia de usuario:** Como **ayudante** deseo consultar mis tareas diarias para asi saber que tengo que hacer en el dia.||
| **Prioridad:** Alta |**Estimación (días):** 3 días |

| **Título:**|**Id:** 15|
|----------------------------|--------------------------------|
| **Historia de usuario:** Como **ayudante** quiero reportar el estado de mis tareas para notificar los resultados de mi trabajo.||
| **Prioridad:** Alta |**Estimación (días):** 3 días |

| **Título:**|**Id:** 16|
|----------------------------|--------------------------------|
| **Historia de usuario:** Como **ayudante** deseo notificar a los demas ayudantes si se necesita mas personal de este tipo para poder completar una tarea.||
| **Prioridad:** Media |**Estimación (días):** 3 días |

| **Título:**|**Id:** 17|
|----------------------------|--------------------------------|
| **Historia de usuario:** Como **ayudante** deseo registrar ingreso y salida de trabajo para asi notificar las horas laborales.||
| **Prioridad:** Alta |**Estimación (días):** 3 días |


