### Integrantes:
1. Bryan Felipe Muñoz Molina  20162020408
2. Cristian Javier Martinez Blanco  20182020155
3. Kevin Santiago Holguín Bello 20191020035

# Ejercicio Inicial Calculadora (Diagramas)

### Diagrama de casos de uso :see_no_evil:
![Diagrama de casos de uso](https://gitlab.com/f1479/fundamentos-de-ingenieria-de-software/-/raw/main/Modelo%20B%C3%A1sico%20Calculadora/Diagrama%20de%20casos%20de%20uso.jpg)
### Diagrama de clases :hear_no_evil:
![Diagrama de clases](https://gitlab.com/f1479/fundamentos-de-ingenieria-de-software/-/raw/main/Modelo%20B%C3%A1sico%20Calculadora/Diagrama%20de%20clases.jpg)
### Diagrama de secuencia :hear_no_evil:
![Diagrama de secuencia](https://gitlab.com/f1479/fundamentos-de-ingenieria-de-software/-/raw/main/Modelo%20B%C3%A1sico%20Calculadora/Diagrama%20de%20secuencia.jpg)