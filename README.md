### Integrantes:
1. Bryan Felipe Muñoz Molina  20162020408
2. Cristian Javier Martinez Blanco  20182020155
3. Kevin Santiago Holguín Bello 20191020035

# Ejercicio Inicial Calculadora

## Requerimientos :relieved:

### Funcionales :see_no_evil:

- El sistema debe permitir al usuario ingresar números que deben interactuar con las operaciones básicas (Suma, resta, Multiplicación, división), generando una salida (número) resultado de la operación.
- El sistema debe estar en capacidad de de manejar diferentes sistemas de numeración como Hexadecimal, Octal, Binario, Decimal.

### No funcionales :hear_no_evil:

- El sistema debe poder interactuar con el usuario por medio del navegador web a través de la interacción del teclado y botones.

## Actividades 
- Selección de tecnologías a usar :triangular_flag_on_post:
- Diseño del aplicativo web :triangular_flag_on_post:
- Maquetación del diseño :triangular_flag_on_post:
- Desarrollo de funcionalidades :triangular_flag_on_post:
- QA (Quality Assurance ) :triangular_flag_on_post:
- Despliegue :triangular_flag_on_post:
- DevOps :triangular_flag_on_post:


## Tiempo

- :dart: ( 8 Horas ) Selección de tecnologías a usar :boom:
- :dart: ( 16 Horas ) Diseño del aplicativo web :boom:
- :dart: ( 5 Horas ) Maquetación del diseño :boom:
- :dart: ( 8 Horas ) Desarrollo de funcionalidades :boom:
- :dart: ( 8 Horas ) QA (Quality Assurance ) :boom:
- :dart: ( 3 Horas ) Despliegue :boom:
- :dart: ( 1 Hora Semanal ) DevOps :triangular_flag_on_post:

## Tiempo total estimado: 49 Horas.

## Precio

- Precio hora: $ 20.000 COP
- Precio total: Precio hora x Tiempo = $ 20.000 COP * 49 =  $ 980.000 COP

# Modelo Básico Calculadora
https://drive.google.com/file/d/1Mgm27X7N44ra1sXDUO7dXhoLz28I4lni/view?usp=sharing

# Otro programa con sus diagramas (Batalla Naval)
https://drive.google.com/file/d/1THDWWCA5N5xsnR-P6S4I1C4o7OZNhxTt/view?usp=sharing
